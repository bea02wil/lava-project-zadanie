﻿using UnityEngine;

[CreateAssetMenu(fileName = "Projectile", menuName = "Stats/Projectile", order = 2)]
public class Projectile_SO : ScriptableObject
{
    [SerializeField] private ProjectileForce projectileForce;
    [SerializeField] private float projectileSpawnSpeed; //projectile speed

    public ProjectileForce ProjectileForce { get => projectileForce; set { projectileForce = value; } }
    public float ProjectileSpawnSpeed { get => projectileSpawnSpeed; set { projectileSpawnSpeed = value; } }
}
