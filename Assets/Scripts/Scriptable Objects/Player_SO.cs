﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerStats", menuName = "Stats/Player", order = 1)]
public class Player_SO : ScriptableObject
{
    [SerializeField] private int playerSpeed;
    
    public int PlayerSpeed { get => playerSpeed; set { playerSpeed = value; } }
}
