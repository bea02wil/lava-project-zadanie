﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : Manager<UIManager>
{
    public Sprite shootingIcon;
    public Sprite noShootingIcon;

    [SerializeField] private Image playerIcon;

    public void UpdatePlayerIcon(Sprite icon)
    {
        playerIcon.sprite = icon;
    }
}
