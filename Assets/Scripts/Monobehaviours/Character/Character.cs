﻿using UnityEngine;
using UnityEngine.AI;

public class Character : MonoBehaviour
{
    protected Animator animator;

    protected NavMeshAgent agent;

    protected virtual void Awake()
    {
        animator = GetComponent<Animator>();

        agent = GetComponent<NavMeshAgent>();
    }
}
