﻿using UnityEngine;

public class Enemy : Character
{
    [SerializeField] private Transform[] wayPoints;
    [SerializeField] private float patrolTime = 10f;

    private int index;

    protected override void Awake()
    {
        base.Awake();

        index = Random.Range(0, wayPoints.Length);

        InvokeRepeating(nameof(Tick), 0, 0.5f);

        InvokeRepeating(nameof(Patrol), 0, patrolTime);
    }

    private void Update()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Z_FallingBack"))
        {
            agent.ResetPath();
            return;
        }
       
        animator.SetFloat("Speed", agent.velocity.sqrMagnitude);
       
        if (agent.velocity.sqrMagnitude < 0.005f)
        {
            animator.SetTrigger("StopEnemyWalkAnim");
        }
    }

    private void Patrol()
    {
        index = index == wayPoints.Length - 1 ? 0 : index + 1;
    }

    private void Tick()
    {
        agent.destination = wayPoints[index].position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Z_FallingBack")) { return; }
    
        animator.SetTrigger("FallBack");
    }
}
