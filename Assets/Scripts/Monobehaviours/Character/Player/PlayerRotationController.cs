﻿using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerRotationController : Player
{
    [SerializeField] private float rotateSpeed = 6f;
 
    private void Update()
    {   
        if (ShootingMode == ShootingMode.None) return;

        if (Physics.Raycast(mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue()),
             maxClickDistance, clickableLayer.value))
        {

            if (Mouse.current.leftButton.wasPressedThisFrame
                || Mouse.current.leftButton.isPressed)
            {
                RotateToAttackClick();
            }

        }       
    }

    private void RotateToAttackClick()
    {
        var mouseWorldPosition = mainCamera.ScreenToWorldPoint(
            (Vector3)Mouse.current.position.ReadValue() + Vector3.forward * rotateSpeed);

        var angle = GetAngleBetweenPoints(transform.position, mouseWorldPosition);

        transform.rotation = Quaternion.Euler(new Vector3(0f, angle, 0f));
    }

    private float GetAngleBetweenPoints(Vector3 firstPoint, Vector3 secondPoint)
    {
        return Mathf.Atan2(secondPoint.x - firstPoint.x,
            secondPoint.y - firstPoint.y) * Mathf.Rad2Deg;
    }
}
