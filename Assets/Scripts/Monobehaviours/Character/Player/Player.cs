﻿using UnityEngine;

public enum ShootingMode
{
    Fire,
    None
}

public class Player : Character
{
    [SerializeField] protected int maxClickDistance;

    [SerializeField] protected Player_SO playerStats;

    [SerializeField] protected LayerMask clickableLayer;
    
    [SerializeField] protected MouseManager mouseManager; 

    protected Camera mainCamera;

    public ShootingMode ShootingMode { get; set; } = ShootingMode.None;

    protected override void Awake()
    {
        base.Awake();

        mainCamera = Camera.main;
    }   

}
