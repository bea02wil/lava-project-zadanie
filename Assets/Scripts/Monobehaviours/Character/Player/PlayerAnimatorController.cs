﻿using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerAnimatorController : Player
{
    private void Update()
    {
        animator.SetFloat("Speed", agent.velocity.sqrMagnitude);
       
        if (ShootingMode == ShootingMode.None) return;        

        if (Physics.Raycast(mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue()),
             maxClickDistance, clickableLayer.value))
        {

            if (mouseManager.WasLeftButtonShortClick)
            {             
                animator.SetTrigger("Attack");

                mouseManager.WasLeftButtonShortClick = false;
            }

            if (mouseManager.WasLeftButtonLongClick)
            {                
                animator.SetBool("AttackHold", true);
                
                mouseManager.WasLeftButtonLongClick = false;
            }
            else
            {              
                AnimatorUnFreezeHand();
            }
        }
        
    }

    //Animation event
    public void AnimatorFreezeHand()
    {
        animator.enabled = false;
    }

    private void AnimatorUnFreezeHand()
    {
        animator.SetBool("AttackHold", false);

        animator.enabled = true;
    }
}
