﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerShootingManager : Player
{   
    [SerializeField] private Transform projectileSpawnPosition;
    [SerializeField] private Projectile projectile;

    private ObjectPool objectPool;

    private void Start()
    {
        objectPool = GetComponent<ObjectPool>();

        projectile.ProjectileSpawnSpeed = projectile.projectileStats.ProjectileSpawnSpeed;
    }


    //Animation event
    public void SingleShot()
    {
        if (Physics.Raycast(mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue()),
            out var hit, maxClickDistance, clickableLayer.value))
        {
            var projectile = objectPool.GetAvailableObject();
            projectile.transform.position = projectileSpawnPosition.position;

            projectile.transform.LookAt(hit.point);
      
            projectile.SetActive(true);
        }
    }

    //Animation event
    public void MultipleShot()
    {
        InvokeRepeating(nameof(SingleShot), 0f, projectile.ProjectileSpawnSpeed);
    }

    //Animation event
    public void StopMultipleShot()
    {
        CancelInvoke(nameof(SingleShot));
    }
}
