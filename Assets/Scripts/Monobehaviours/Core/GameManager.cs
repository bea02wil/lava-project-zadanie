﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameManager : Manager<GameManager>
{
    [SerializeField] private Player player;

    public Action<ShootingMode, Sprite> PlayerReachedWeaponAreaPosition;

    protected override void Awake()
    {
        base.Awake();

        PlayerReachedWeaponAreaPosition = OnPlayerReachedWeaponAreaPosition;
    }

    private void Update()
    {
        if (Keyboard.current[Key.R].wasPressedThisFrame)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }

    private void OnPlayerReachedWeaponAreaPosition(ShootingMode shootingMode, Sprite icon)
    {
        player.GetComponent<PlayerShootingManager>().ShootingMode = shootingMode;

        player.GetComponent<PlayerAnimatorController>().ShootingMode = shootingMode;

        player.GetComponent<PlayerAgentController>().ShootingMode = shootingMode;

        player.GetComponent<PlayerRotationController>().ShootingMode = shootingMode;

        UIManager.Instance.UpdatePlayerIcon(icon);
    }
}
