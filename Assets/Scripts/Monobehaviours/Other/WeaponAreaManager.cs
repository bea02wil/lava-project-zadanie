﻿using UnityEngine;

public class WeaponAreaManager : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.Instance.PlayerReachedWeaponAreaPosition?
                .Invoke(ShootingMode.Fire, UIManager.Instance.shootingIcon);
        }        
    }
}
