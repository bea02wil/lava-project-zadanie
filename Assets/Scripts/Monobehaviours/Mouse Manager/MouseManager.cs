﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;


[Serializable]
public class EventMove : UnityEvent<Vector3> { }

public class MouseManager : MonoBehaviour
{     
    [SerializeField] private EventMove OnClickEnviroment;

    [SerializeField] private int maxClickDistance = 30;
    [SerializeField] private LayerMask clickableLayer;
    
    public bool WasLeftButtonShortClick { get; set; }
    public bool WasLeftButtonLongClick { get; set; }

    private float totalDownTime;
    private float leftClickDuration = 0.2f;

    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    private void Update()
    {
        if (Physics.Raycast(mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue()),
            out var hit, maxClickDistance, clickableLayer.value))
        {
            if (Mouse.current.rightButton.wasPressedThisFrame)
            {
                OnClickEnviroment?.Invoke(hit.point);
            }
        }

        if (Mouse.current.leftButton.wasPressedThisFrame)
        {
            totalDownTime = Time.time;
        }

        if (Mouse.current.leftButton.isPressed
            && (Time.time - totalDownTime) > leftClickDuration)
        {
            WasLeftButtonLongClick = true;
        }

        else if (Mouse.current.leftButton.wasReleasedThisFrame
            && (Time.time - totalDownTime) < leftClickDuration)
        {
            WasLeftButtonShortClick = true;
        }
    }
}
