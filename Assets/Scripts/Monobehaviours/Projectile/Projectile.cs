﻿using UnityEngine;

public enum ProjectileForce
{
    Low = 5,
    Medium = 10,
    High = 15
}

public class Projectile : MonoBehaviour
{
    public Projectile_SO projectileStats;

    public float ProjectileSpawnSpeed { get; set; }

    private int projectileForce;

    private void OnEnable()
    {
        projectileForce = (int)projectileStats.ProjectileForce;
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * projectileForce);
    }

    private void OnTriggerEnter(Collider other)
    {
        gameObject.SetActive(false);
    }
}
